package com.leodrumers.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var diceImage : ImageView
    lateinit var diceImage2 : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton : Button = findViewById(R.id.roll_button)
        rollButton.setOnClickListener {
            rollDices()
        }

        diceImage = findViewById(R.id.diceImageView)
        diceImage2 = findViewById(R.id.diceImageView2)
    }

    private fun rollDices() {
        val drawableDiceResource = getDrawableResource()
        val drawableDiceResource2 = getDrawableResource()
        diceImage.setImageResource(drawableDiceResource)
        diceImage2.setImageResource(drawableDiceResource2)
    }

    private fun getDrawableResource(): Int {
        return when (Random().nextInt(6) + 1) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
    }
}
